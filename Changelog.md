**NOTE**

All changes are based on mutt-kz by Karel Zak from (5147a6c19) Wed, 22 Jul 2015

**NOTE**

1.6.0:

+ mutt/core: use struct content_state instead of CONTENT_STATE
+ mutt/core: use struct parameter instead of PARAMETER
+ mutt/core: use struct envelope instead of ENVELOPE
+ mutt/core: use struct spam_list instead of SPAM_LIST
+ mutt/core: use struct rx_list instead of typedef RX_LIST
+ mutt/core: use struct event instead of typedef event_t
+ mutt/core: use struct regexp instead of typedef REGEXP
+ mutt/core: crypto code moved to the crypto/
+ mutt/core: use struct buffy instead of typedef BUFFY
+ mutt/build: provide $(MUTT_LIBS) make variable which contains all local libs
+ mutt/build: move all rfc* code to the rfc directory and build librfc.a from it
+ mutt/cli: Handle '-Q' command line arguments directly in CLA loop
+ mutt/cli: Handle '-A' command line argument right after parsing
+ mutt/doc: add manual pages
+ mutt/core: remove strnlen.c
+ specify newMagic in the command line arguments handling
+ mutt/core: remove strnlen.c
+ mutt/core: use struct alias instead of ALIAS
+ mutt/core: remove unnecesary dump_variables variable
+ mutt/core: set locale LC_ALL
+ mutt/core: use struct address instead of ADDRESS
+ mutt/core: use struct address instead of ADDRESS
+ Use `struct address` instead of `ADDRESS`
+ Get rid of getopt
+ Use  `struct option` instead of `Options` and `struct quad_options` instead of `QuadOptions`
+ Move `Options` and `QuadOptions` to "mutt.h"
+ Use srand48 and lrand48 instead of macros
+ Use <sysexits.h> instead of custom EX_.* macros
+ Remove ABOUT-NLS file
+ Get rid of two translation helpers: `_` and `_N`
+ Get rid of FALSE/TRUE macro
+ Get rid of extlib.c and string helpers (no need in it, all of they are provided by glibc)
+ Get rid of NLS
+ Use `struct list_t` insted of `LIST`
+ Use `struct header` instead of `HEADER`
+ Glibc provides <limits.h>, now need to check it
+ Get rid of LOFF_T and OFF_T_FMT macros
+ Get rid of GNU codding style. Use https://www.kernel.org/doc/Documentation/CodingStyle
+ Move on plain Makefile instead of autoconf
+ Get rid of POP3 implementation
