/*
 * Copyright(C) 2004 Thomas Glanzmann <sithglan@stud.uni-erlangen.de>
 * Copyright(C) 2004 Tobias Werth <sitowert@stud.uni-erlangen.de>
 * Copyright(C) 2004 Brian Fundakowski Feldman <green@FreeBSD.org>
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "mutt.h"

#if USE_HCACHE
#define HAVE_GDBM 1
#endif

#if HAVE_GDBM
#include <gdbm.h>
#endif

#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>
#include "hcache.h"
#include "mx.h"
#include "lib.h"
#include "md5.h"
#include "rfc822.h"

unsigned int hcachever = 0x0;

#if HAVE_GDBM
struct header_cache
{
	GDBM_FILE db;
	char *folder;
	unsigned int crc;
};
#endif

typedef union
{
	struct timeval timeval;
	unsigned int uidvalidity;
} validate;

static void *
lazy_malloc(size_t siz)
{
	if (0 < siz && siz < 4096)
		siz = 4096;

	return safe_malloc(siz);
}

static void
lazy_realloc(void *ptr, size_t siz)
{
	void **p =(void **) ptr;

	if (p != NULL && 0 < siz && siz < 4096)
		return;

	safe_realloc(ptr, siz);
}

static unsigned char *
dump_int(unsigned int i, unsigned char *d, int *off)
{
	lazy_realloc(&d, *off + sizeof(int));
	memcpy(d + *off, &i, sizeof(int));
	(*off) += sizeof(int);

	return d;
}

static void
restore_int(unsigned int *i, const unsigned char *d, int *off)
{
	memcpy(i, d + *off, sizeof(int));
	(*off) += sizeof(int);
}

static inline int is_ascii(const char *p, size_t len) {
	register const char *s = p;
	while(s &&(unsigned)(s - p) < len) {
		if ((*s & 0x80) != 0)
			return 0;
		s++;
	}
	return 1;
}

static unsigned char *
dump_char_size(char *c, unsigned char *d, int *off, ssize_t size, int convert)
{
	char *p = c;

	if (c == NULL)
	{
		size = 0;
		d = dump_int(size, d, off);
		return d;
	}

	if (convert && !is_ascii(c, size)) {
		p = mutt_substrdup(c, c + size);
		if (mutt_convert_string(&p, Charset, "utf-8", 0) == 0) {
			c = p;
			size = mutt_strlen(c) + 1;
		}
	}

	d = dump_int(size, d, off);
	lazy_realloc(&d, *off + size);
	memcpy(d + *off, p, size);
	*off += size;

	if (p != c)
		safe_free(&p);

	return d;
}

static unsigned char *
dump_char(char *c, unsigned char *d, int *off, int convert)
{
	return dump_char_size(c, d, off, mutt_strlen(c) + 1, convert);
}

static void
restore_char(char **c, const unsigned char *d, int *off, int convert)
{
	unsigned int size;
	restore_int(&size, d, off);

	if (size == 0)
	{
		*c = NULL;
		return;
	}

	*c = safe_malloc(size);
	memcpy(*c, d + *off, size);
	if (convert && !is_ascii(*c, size)) {
		char *tmp = safe_strdup(*c);
		if (mutt_convert_string(&tmp, "utf-8", Charset, 0) == 0) {
			mutt_str_replace(c, tmp);
		} else {
			safe_free(&tmp);
		}
	}
	*off += size;
}

static unsigned char *
dump_address(struct address * a, unsigned char *d, int *off, int convert)
{
	unsigned int counter = 0;
	unsigned int start_off = *off;

	d = dump_int(0xdeadbeef, d, off);

	while(a)
	{
#if EXACT_ADDRESS
		d = dump_char(a->val, d, off, convert);
#endif
		d = dump_char(a->personal, d, off, convert);
		d = dump_char(a->mailbox, d, off, 0);
		d = dump_int(a->group, d, off);
		a = a->next;
		counter++;
	}

	memcpy(d + start_off, &counter, sizeof(int));

	return d;
}

static void
restore_address(struct address **a, const unsigned char *d, int *off, int convert)
{
	unsigned int counter;

	restore_int(&counter, d, off);

	while(counter)
	{
		*a = rfc822_new_address();
#if EXACT_ADDRESS
		restore_char(&(*a)->val, d, off, convert);
#endif
		restore_char(&(*a)->personal, d, off, convert);
		restore_char(&(*a)->mailbox, d, off, 0);
		restore_int((unsigned int *) &(*a)->group, d, off);
		a = &(*a)->next;
		counter--;
	}

	*a = NULL;
}

static unsigned char *
dump_list(struct list_t * l, unsigned char *d, int *off, int convert)
{
	unsigned int counter = 0;
	unsigned int start_off = *off;

	d = dump_int(0xdeadbeef, d, off);

	while(l)
	{
		d = dump_char(l->data, d, off, convert);
		l = l->next;
		counter++;
	}

	memcpy(d + start_off, &counter, sizeof(int));

	return d;
}

static void
restore_list(struct list_t **l, const unsigned char *d, int *off, int convert)
{
	unsigned int counter;

	restore_int(&counter, d, off);

	while(counter)
	{
		*l = safe_malloc(sizeof(struct list_t));
		restore_char(&(*l)->data, d, off, convert);
		l = &(*l)->next;
		counter--;
	}

	*l = NULL;
}

static unsigned char *
dump_buffer(struct buffer *b, unsigned char *d, int *off, int convert)
{
	if (!b)
	{
		d = dump_int(0, d, off);
		return d;
	}
	else
		d = dump_int(1, d, off);

	d = dump_char_size(b->data, d, off, b->dsize + 1, convert);
	d = dump_int(b->dptr - b->data, d, off);
	d = dump_int(b->dsize, d, off);
	d = dump_int(b->destroy, d, off);

	return d;
}

static void
restore_buffer(struct buffer **b, const unsigned char *d, int *off, int convert)
{
	unsigned int used;
	unsigned int offset;
	restore_int(&used, d, off);
	if (!used)
	{
		return;
	}

	*b = safe_malloc(sizeof(struct buffer));

	restore_char(&(*b)->data, d, off, convert);
	restore_int(&offset, d, off);
	(*b)->dptr =(*b)->data + offset;
	restore_int(&used, d, off);
	(*b)->dsize = used;
	restore_int(&used, d, off);
	(*b)->destroy = used;
}

static unsigned char *
dump_parameter(struct parameter *p, unsigned char *d, int *off, int convert)
{
	unsigned int counter = 0;
	unsigned int start_off = *off;

	d = dump_int(0xdeadbeef, d, off);

	while(p)
	{
		d = dump_char(p->attribute, d, off, 0);
		d = dump_char(p->value, d, off, convert);
		p = p->next;
		counter++;
	}

	memcpy(d + start_off, &counter, sizeof(int));

	return d;
}

static void
restore_parameter(struct parameter **p, const unsigned char *d, int *off, int convert)
{
	unsigned int counter;

	restore_int(&counter, d, off);

	while(counter)
	{
		*p = safe_malloc(sizeof(struct parameter));
		restore_char(&(*p)->attribute, d, off, 0);
		restore_char(&(*p)->value, d, off, convert);
		p = &(*p)->next;
		counter--;
	}

	*p = NULL;
}

static unsigned char *
dump_body(struct body *c, unsigned char *d, int *off, int convert)
{
	struct body nb;

	memcpy(&nb, c, sizeof(struct body));

	/* some fields are not safe to cache */
	nb.content = NULL;
	nb.charset = NULL;
	nb.next = NULL;
	nb.parts = NULL;
	nb.hdr = NULL;
	nb.aptr = NULL;

	lazy_realloc(&d, *off + sizeof(struct body));
	memcpy(d + *off, &nb, sizeof(struct body));
	*off += sizeof(struct body);

	d = dump_char(nb.xtype, d, off, 0);
	d = dump_char(nb.subtype, d, off, 0);

	d = dump_parameter(nb.parameter, d, off, convert);

	d = dump_char(nb.description, d, off, convert);
	d = dump_char(nb.form_name, d, off, convert);
	d = dump_char(nb.filename, d, off, convert);
	d = dump_char(nb.d_filename, d, off, convert);

	return d;
}

static void
restore_body(struct body * c, const unsigned char *d, int *off, int convert)
{
	memcpy(c, d + *off, sizeof(struct body));
	*off += sizeof(struct body);

	restore_char(&c->xtype, d, off, 0);
	restore_char(&c->subtype, d, off, 0);

	restore_parameter(&c->parameter, d, off, convert);

	restore_char(&c->description, d, off, convert);
	restore_char(&c->form_name, d, off, convert);
	restore_char(&c->filename, d, off, convert);
	restore_char(&c->d_filename, d, off, convert);
}

static unsigned char *
dump_envelope(struct envelope *e, unsigned char *d, int *off, int convert)
{
	d = dump_address(e->return_path, d, off, convert);
	d = dump_address(e->from, d, off, convert);
	d = dump_address(e->to, d, off, convert);
	d = dump_address(e->cc, d, off, convert);
	d = dump_address(e->bcc, d, off, convert);
	d = dump_address(e->sender, d, off, convert);
	d = dump_address(e->reply_to, d, off, convert);
	d = dump_address(e->mail_followup_to, d, off, convert);

	d = dump_char(e->list_post, d, off, convert);
	d = dump_char(e->subject, d, off, convert);

	if (e->real_subj)
		d = dump_int(e->real_subj - e->subject, d, off);
	else
		d = dump_int(-1, d, off);

	d = dump_char(e->message_id, d, off, 0);
	d = dump_char(e->supersedes, d, off, 0);
	d = dump_char(e->date, d, off, 0);
	d = dump_char(e->x_label, d, off, convert);

	d = dump_buffer(e->spam, d, off, convert);

	d = dump_list(e->references, d, off, 0);
	d = dump_list(e->in_reply_to, d, off, 0);
	d = dump_list(e->userhdrs, d, off, convert);

	return d;
}

static void
restore_envelope(struct envelope *e, const unsigned char *d, int *off, int convert)
{
	int real_subj_off;

	restore_address(&e->return_path, d, off, convert);
	restore_address(&e->from, d, off, convert);
	restore_address(&e->to, d, off, convert);
	restore_address(&e->cc, d, off, convert);
	restore_address(&e->bcc, d, off, convert);
	restore_address(&e->sender, d, off, convert);
	restore_address(&e->reply_to, d, off, convert);
	restore_address(&e->mail_followup_to, d, off, convert);

	restore_char(&e->list_post, d, off, convert);
	restore_char(&e->subject, d, off, convert);
	restore_int((unsigned int *)(&real_subj_off), d, off);

	if (0 <= real_subj_off)
		e->real_subj = e->subject + real_subj_off;
	else
		e->real_subj = NULL;

	restore_char(&e->message_id, d, off, 0);
	restore_char(&e->supersedes, d, off, 0);
	restore_char(&e->date, d, off, 0);
	restore_char(&e->x_label, d, off, convert);

	restore_buffer(&e->spam, d, off, convert);

	restore_list(&e->references, d, off, 0);
	restore_list(&e->in_reply_to, d, off, 0);
	restore_list(&e->userhdrs, d, off, convert);
}

static int
crc_matches(const char *d, unsigned int crc)
{
	int off = sizeof(validate);
	unsigned int mycrc = 0;

	if (!d)
		return 0;

	restore_int(&mycrc,(unsigned char *) d, &off);

	return(crc == mycrc);
}

/* Append md5sumed folder to path if path is a directory. */
static const char *
mutt_hcache_per_folder(const char *path, const char *folder,
                       hcache_namer_t namer)
{
	static char hcpath[_POSIX_PATH_MAX];
	struct stat sb;
	unsigned char md5sum[16];
	char* s;
	int ret, plen;
#ifndef HAVE_ICONV
	const char *chs = Charset && *Charset ? Charset :
		mutt_get_default_charset();
#endif

	plen = mutt_strlen(path);

	ret = stat(path, &sb);
	if (ret < 0 && path[plen-1] != '/')
	{
#if HAVE_ICONV
		return path;
#else
		snprintf(hcpath, _POSIX_PATH_MAX, "%s-%s", path, chs);
		return hcpath;
#endif
	}

	if (ret >= 0 && !S_ISDIR(sb.st_mode))
	{
#if HAVE_ICONV
		return path;
#else
		snprintf(hcpath, _POSIX_PATH_MAX, "%s-%s", path, chs);
		return hcpath;
#endif
	}

	if (namer)
	{
		snprintf(hcpath, sizeof(hcpath), "%s%s", path,
			  path[plen-1] == '/' ? "" : "/");
		if (path[plen-1] != '/')
			plen++;

		ret = namer(folder, hcpath + plen, sizeof(hcpath) - plen);
	}
	else
	{
		md5_buffer(folder, strlen(folder), &md5sum);

		ret = snprintf(hcpath, _POSIX_PATH_MAX,
			       "%s/%02x%02x%02x%02x%02x%02x%02x%02x"
			       "%02x%02x%02x%02x%02x%02x%02x%02x"
#ifndef HAVE_ICONV
			       "-%s"
#endif
			       ,
			       path, md5sum[0], md5sum[1], md5sum[2], md5sum[3],
			       md5sum[4], md5sum[5], md5sum[6], md5sum[7], md5sum[8],
			       md5sum[9], md5sum[10], md5sum[11], md5sum[12],
			       md5sum[13], md5sum[14], md5sum[15]
#ifndef HAVE_ICONV
			       ,chs
#endif
			);
	}

	if (ret <= 0)
		return path;

	if (stat(hcpath, &sb) >= 0)
		return hcpath;

	s = strchr(hcpath + 1, '/');
	while(s)
	{
		/* create missing path components */
		*s = '\0';
		if (stat(hcpath, &sb) < 0 &&(errno != ENOENT || mkdir(hcpath, 0777) < 0))
			return path;
		*s = '/';
		s = strchr(s + 1, '/');
	}

	return hcpath;
}

/* This function transforms a header into a char so that it is useable by
 * db_store.
 */
static void *
mutt_hcache_dump(header_cache_t *h, struct header *header, int *off,
		 unsigned int uidvalidity, mutt_hcache_store_flags_t flags)
{
	unsigned char *d = NULL;
	struct header nh;
	int convert = !Charset_is_utf8;

	*off = 0;
	d = lazy_malloc(sizeof(validate));

	if (flags & M_GENERATE_UIDVALIDITY)
	{
		struct timeval now;
		gettimeofday(&now, NULL);
		memcpy(d, &now, sizeof(struct timeval));
	}
	else
		memcpy(d, &uidvalidity, sizeof(uidvalidity));
	*off += sizeof(validate);

	d = dump_int(h->crc, d, off);

	lazy_realloc(&d, *off + sizeof(struct header));
	memcpy(&nh, header, sizeof(struct header));

	/* some fields are not safe to cache */
	nh.tagged = 0;
	nh.changed = 0;
	nh.threaded = 0;
	nh.recip_valid = 0;
	nh.searched = 0;
	nh.matched = 0;
	nh.collapsed = 0;
	nh.limited = 0;
	nh.num_hidden = 0;
	nh.recipient = 0;
	nh.pair = 0;
	nh.attach_valid = 0;
	nh.path = NULL;
	nh.tree = NULL;
	nh.thread = NULL;
#if USE_IMAP
	nh.data = NULL;
#endif

	memcpy(d + *off, &nh, sizeof(struct header));
	*off += sizeof(struct header);

	d = dump_envelope(nh.env, d, off, convert);
	d = dump_body(nh.content, d, off, convert);
	d = dump_char(nh.maildir_flags, d, off, convert);

	return d;
}

struct header *
mutt_hcache_restore(const unsigned char *d, struct header **oh)
{
	int off = 0;
	struct header *h = mutt_new_header();
	int convert = !Charset_is_utf8;

	/* skip validate */
	off += sizeof(validate);

	/* skip crc */
	off += sizeof(unsigned int);

	memcpy(h, d + off, sizeof(struct header));
	off += sizeof(struct header);

	h->env = mutt_new_envelope();
	restore_envelope(h->env, d, &off, convert);

	h->content = mutt_new_body();
	restore_body(h->content, d, &off, convert);

	restore_char(&h->maildir_flags, d, &off, convert);

	/* this is needed for maildir style mailboxes */
	if (oh)
	{
		h->old =(*oh)->old;
		h->path = safe_strdup((*oh)->path);
		mutt_free_header(oh);
	}

	return h;
}

void *
mutt_hcache_fetch(header_cache_t *h, const char *filename,
		  size_t(*keylen)(const char *fn))
{
	void* data;

	data = mutt_hcache_fetch_raw(h, filename, keylen);

	if (!data || !crc_matches(data, h->crc))
	{
		safe_free(&data);
		return NULL;
	}

	return data;
}

void *
mutt_hcache_fetch_raw(header_cache_t *h, const char *filename,
                       size_t(*keylen)(const char *fn))
{
	char path[_POSIX_PATH_MAX];
	int ksize;

#if HAVE_GDBM
	datum key;
	datum data;
#endif

	if (!h)
		return NULL;

	strncpy(path, h->folder, sizeof (path));
	safe_strcat(path, sizeof (path), filename);

	ksize = strlen (h->folder) + keylen (path + strlen (h->folder));

#if HAVE_GDBM
	key.dptr = path;
	key.dsize = ksize;
	data = gdbm_fetch(h->db, key);
	return data.dptr;
#endif
}

/*
 * flags
 *
 * M_GENERATE_UIDVALIDITY
 * ignore uidvalidity param and store gettimeofday() as the value
 */
int
mutt_hcache_store(header_cache_t *h, const char *filename, struct header *header,
		  unsigned int uidvalidity,
		  size_t(*keylen)(const char *fn),
		  mutt_hcache_store_flags_t flags)
{
	char* data;
	int dlen;
	int ret;

	if (!h)
		return -1;

	data = mutt_hcache_dump(h, header, &dlen, uidvalidity, flags);
	ret = mutt_hcache_store_raw(h, filename, data, dlen, keylen);

	safe_free(&data);

	return ret;
}

int
mutt_hcache_store_raw(header_cache_t* h, const char* filename, void* data,
                       size_t dlen, size_t(*keylen)(const char* fn))
{
	char path[_POSIX_PATH_MAX];
	int ksize;
#if HAVE_GDBM
	datum key;
	datum databuf;
#endif
	if (!h)
		return -1;
	strncpy(path, h->folder, sizeof (path));
	safe_strcat(path, sizeof (path), filename);
	ksize = strlen(h->folder) + keylen(path + strlen(h->folder));
#if HAVE_GDBM
	key.dptr = path;
	key.dsize = ksize;
	databuf.dsize = dlen;
	databuf.dptr = data;
	return gdbm_store(h->db, key, databuf, GDBM_REPLACE);
#endif
}

static char* get_foldername(const char *folder)
{
	char *p = NULL;
	char path[_POSIX_PATH_MAX];
	struct stat st;

	mutt_encode_path(path, sizeof(path), folder);

	/* if the folder is local, canonify the path to avoid
	 * to ensure equivalent paths share the hcache */
	if (stat(path, &st) == 0)
	{
		p = safe_malloc(PATH_MAX+1);
		if (!realpath(path, p))
			mutt_str_replace(&p, path);
	} else
		p = safe_strdup(path);

	return p;
}

#if HAVE_GDBM
static int
hcache_open_gdbm(struct header_cache* h, const char* path)
{
	int pagesize;

	if (mutt_atoi(HeaderCachePageSize, &pagesize) < 0 || pagesize <= 0)
		pagesize = 16384;

	h->db = gdbm_open((char *) path, pagesize, GDBM_WRCREAT, 00600, NULL);
	if (h->db)
		return 0;

	/* if rw failed try ro */
	h->db = gdbm_open((char *) path, pagesize, GDBM_READER, 00600, NULL);
	if (h->db)
		return 0;

	return -1;
}

void
mutt_hcache_close(header_cache_t *h)
{
	if (!h)
		return;

	gdbm_close(h->db);
	safe_free(&h->folder);
	safe_free(&h);
}

int
mutt_hcache_delete(header_cache_t *h, const char *filename,
		   size_t(*keylen)(const char *fn))
{
	datum key;
	char path[_POSIX_PATH_MAX];

	if (!h)
		return -1;

	strncpy(path, h->folder, sizeof(path));
	safe_strcat(path, sizeof(path), filename);

	key.dptr = path;
	key.dsize = strlen(h->folder) + keylen(path + strlen(h->folder));

	return gdbm_delete(h->db, key);
}
#endif

header_cache_t *
mutt_hcache_open(const char *path, const char *folder, hcache_namer_t namer)
{
	struct header_cache *h = safe_calloc(1, sizeof(struct header_cache));
	int(*hcache_open)(struct header_cache* h, const char* path);
	struct stat sb;

#if HAVE_GDBM
	hcache_open = hcache_open_gdbm;
#endif

	/* Calculate the current hcache version from dynamic configuration */
	if (hcachever == 0x0) {
		union {
			unsigned char charval[16];
			unsigned int intval;
		} digest;
		struct md5_ctx ctx;
		struct spam_list *spam;
		struct rx_list *nospam;

		hcachever = HCACHEVER;

		md5_init_ctx(&ctx);

		/* Seed with the compiled-in header structure hash */
		md5_process_bytes(&hcachever, sizeof(hcachever), &ctx);

		/* Mix in user's spam list */
		for(spam = SpamList; spam; spam = spam->next)
		{
			md5_process_bytes(spam->rx->pattern, strlen(spam->rx->pattern), &ctx);
			md5_process_bytes(spam->template, strlen(spam->template), &ctx);
		}

		/* Mix in user's nospam list */
		for(nospam = NoSpamList; nospam; nospam = nospam->next)
		{
			md5_process_bytes(nospam->rx->pattern, strlen(nospam->rx->pattern), &ctx);
		}

		/* Get a hash and take its bytes as an(unsigned int) hash version */
		md5_finish_ctx(&ctx, digest.charval);
		hcachever = digest.intval;
	}

	h->db = NULL;
	h->folder = get_foldername(folder);
	h->crc = hcachever;

	if (!path || path[0] == '\0')
	{
		safe_free(&h->folder);
		safe_free(&h);
		return NULL;
	}

	path = mutt_hcache_per_folder(path, h->folder, namer);

	if (!hcache_open(h, path))
		return h;
	else
	{
		/* remove a possibly incompatible version */
		if (!stat(path, &sb) && !unlink(path))
		{
			if (!hcache_open(h, path))
				return h;
		}
		safe_free(&h->folder);
		safe_free(&h);

		return NULL;
	}
}

#if HAVE_GDBM
const char *mutt_hcache_backend(void)
{
	return gdbm_version;
}
#endif
