/*
 * Copyright(C) 2000 Edmund Grimley Evans <edmundo@rano.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/*
 * Japanese support by TAKIZAWA Takashi <taki@luna.email.ne.jp>.
 */

#include "mutt.h"
#include "mbyte.h"
#include "charset.h"
#include "bitmap.h"

#include <errno.h>
#include <ctype.h>

#ifndef EILSEQ
#define EILSEQ EINVAL
#endif

int Charset_is_utf8 = 0;

void mutt_set_charset(char *charset)
{
	char buffer[STRING];

	mutt_canonical_charset(buffer, sizeof(buffer), charset);

	Charset_is_utf8 = 0;
	if (mutt_is_utf8(buffer))
		Charset_is_utf8 = 1;

	//bind_textdomain_codeset(PACKAGE, buffer);
}

wchar_t replacement_char(void)
{
	return Charset_is_utf8 ? 0xfffd : '?';
}

int mutt_filter_unprintable(char **s)
{
	struct buffer *b = NULL;
	wchar_t wc;
	size_t k, k2;
	char scratch[MB_LEN_MAX + 1];
	char *p = *s;
	mbstate_t mbstate1, mbstate2;

	if (!(b = mutt_buffer_new()))
		return -1;
	memset(&mbstate1, 0, sizeof(mbstate1));
	memset(&mbstate2, 0, sizeof(mbstate2));
	for(;(k = mbrtowc(&wc, p, MB_LEN_MAX, &mbstate1)); p += k)
	{
		if (k ==(size_t)(-1) || k ==(size_t)(-2))
		{
			k = 1;
			memset(&mbstate1, 0, sizeof(mbstate1));
			wc = replacement_char();
		}
		if (!IsWPrint(wc))
			wc = '?';
		k2 = wcrtomb(scratch, wc, &mbstate2);
		scratch[k2] = '\0';
		mutt_buffer_addstr(b, scratch);
	}
	safe_free(s);
	*s = b->data ? b->data : safe_calloc(1, 1);
	safe_free(&b);
	return 0;
}
