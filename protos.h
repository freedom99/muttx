/*
 * Copyright (C) 1996-2000,2007 Michael R. Elkins <me@mutt.org>
 * Copyright (C) 2013 Karel Zak <kzak@redhat.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <inttypes.h>

#include "mbyte.h"

#define MoreArgs(p) (*p->dptr && *p->dptr != ';' && *p->dptr != '#')

#define mutt_make_string(A,B,C,D,E) _mutt_make_string(A,B,C,D,E,0)
void _mutt_make_string (char *, size_t, const char *, CONTEXT *,
			struct header *, format_flag);

struct hdr_format_info
{
	CONTEXT *ctx;
	struct header *hdr;
	const char *pager_progress;
};

void mutt_make_string_info (char *, size_t, const char *, struct hdr_format_info *, format_flag);

int mutt_extract_token (struct buffer *, struct buffer *, int);
struct buffer *mutt_buffer_new (void);
struct buffer *mutt_buffer_init (struct buffer *);
struct buffer *mutt_buffer_from (char *);
void mutt_buffer_free(struct buffer **);
int mutt_buffer_printf (struct buffer*, const char*, ...);
void mutt_buffer_add (struct buffer*, const char*, size_t);
void mutt_buffer_addstr (struct buffer*, const char*);
void mutt_buffer_addch (struct buffer*, char);
void mutt_free_opts (void);

#define mutt_system(x) _mutt_system(x,0)
int _mutt_system (const char *, int);

#define mutt_next_thread(x) _mutt_aside_thread(x,1,0)
#define mutt_previous_thread(x) _mutt_aside_thread(x,0,0)
#define mutt_next_subthread(x) _mutt_aside_thread(x,1,1)
#define mutt_previous_subthread(x) _mutt_aside_thread(x,0,1)
int _mutt_aside_thread (struct header *, short, short);

#define mutt_collapse_thread(x,y) _mutt_traverse_thread (x,y,M_THREAD_COLLAPSE)
#define mutt_uncollapse_thread(x,y) _mutt_traverse_thread (x,y,M_THREAD_UNCOLLAPSE)
#define mutt_get_hidden(x,y)_mutt_traverse_thread (x,y,M_THREAD_GET_HIDDEN)
#define mutt_thread_contains_unread(x,y) _mutt_traverse_thread (x,y,M_THREAD_UNREAD)
#define mutt_thread_next_unread(x,y) _mutt_traverse_thread(x,y,M_THREAD_NEXT_UNREAD)
int _mutt_traverse_thread (CONTEXT *ctx, struct header *hdr, int flag);


#define mutt_new_parameter() safe_calloc (1, sizeof (struct parameter))
#define mutt_new_header() safe_calloc (1, sizeof (struct header))
#define mutt_new_envelope() safe_calloc (1, sizeof (struct envelope))
#define mutt_new_enter_state() safe_calloc (1, sizeof (struct enter_state))

typedef const char * format_t (char *, size_t, size_t, char, const char *, const char *, const char *, const char *, unsigned long, format_flag);

void mutt_FormatString (char *, size_t, size_t, const char *, format_t *, unsigned long, format_flag);
void mutt_parse_content_type (char *, struct body *);
void mutt_generate_boundary (struct parameter **);
void mutt_delete_parameter (const char *attribute, struct parameter **p);
void mutt_set_parameter (const char *, const char *, struct parameter **);

FILE *mutt_open_read (const char *, pid_t *);

char* mutt_extract_message_id (const char *, const char **);

struct address *mutt_default_from (void);
struct address *mutt_get_address (struct envelope *, char **);
struct address *mutt_lookup_alias (const char *s);
struct address *mutt_remove_duplicates (struct address *);
struct address *mutt_remove_xrefs (struct address *, struct address *);
struct address *mutt_expand_aliases (struct address *);
struct address *mutt_parse_adrlist (struct address *, const char *);

struct body *mutt_make_file_attach (const char *);
struct body *mutt_make_message_attach (CONTEXT *, struct header *, int);
struct body *mutt_remove_multipart (struct body *);
struct body *mutt_make_multipart (struct body *);
struct body *mutt_new_body (void);
struct body *mutt_parse_multipart (FILE *, const char *, off_t, int);
struct body *mutt_parse_messageRFC822 (FILE *, struct body *);
struct body *mutt_read_mime_header (FILE *, int);

struct content *mutt_get_content_info (const char *fname, struct body *b);

HASH *mutt_make_id_hash (CONTEXT *);
HASH *mutt_make_subj_hash (CONTEXT *);

struct list_t *mutt_make_references(struct envelope *e);

char *mutt_read_rfc822_line (FILE *, char *, size_t *);
struct envelope *mutt_read_rfc822_header (FILE *, struct header *, short, short);
struct header *mutt_dup_header (struct header *);

void mutt_set_mtime (const char *from, const char *to);
time_t mutt_decrease_mtime (const char *, struct stat *);
time_t mutt_local_tz (time_t);
time_t mutt_mktime (struct tm *, int);
time_t mutt_parse_date (const char *, struct header *);
int is_from (const char *, char *, size_t, time_t *);

const char *mutt_attach_fmt (
	char *dest,
	size_t destlen,
	size_t col,
	char op,
	const char *src,
	const char *prefix,
	const char *ifstring,
	const char *elsestring,
	unsigned long data,
	format_flag flags);


char *mutt_charset_hook (const char *);
char *mutt_iconv_hook (const char *);
char *mutt_expand_path (char *, size_t);
char *_mutt_expand_path (char *, size_t, int);
char *mutt_find_hook (int, const char *);
char *mutt_gecos_name (char *, size_t, struct passwd *);
char *mutt_gen_msgid (void);
char *mutt_get_body_charset (char *, size_t, struct body *);
const char *mutt_get_name (struct address *);
char *mutt_get_parameter (const char *, struct parameter *);
struct list_t *mutt_crypt_hook (struct address *);
char *mutt_make_date (char *, size_t);

const char *mutt_make_version(void);

const char *mutt_fqdn(short);

group_t *mutt_pattern_group (const char *);

struct regexp *mutt_compile_regexp (const char *, int);

void mutt_account_hook(const char* url);
void mutt_add_to_reference_headers(struct envelope *env, struct envelope *curenv, struct list_t ***pp, struct list_t ***qq);
void mutt_adv_mktemp(char *, size_t);
void mutt_alias_menu(char *, size_t, struct alias *);
void mutt_allow_interrupt (int);
void mutt_attach_init(struct body *);
void mutt_block_signals(void);
void mutt_block_signals_system(void);
int mutt_body_handler(struct body *, STATE *);
int  mutt_bounce_message(FILE *fp, struct header *, struct address *);
void mutt_break_thread(struct header *);
void mutt_buffy(char *, size_t);
int  mutt_buffy_list(void);
void mutt_canonical_charset(char *, size_t, const char *);
int mutt_count_body_parts(CONTEXT *, struct header *);
void mutt_check_rescore(CONTEXT *);
void mutt_clear_error(void);
void mutt_create_alias(struct envelope *, struct address *);
void mutt_decode_attachment(struct body *, STATE *);
void mutt_decode_base64(STATE *s, long len, int istext, void *cd);
void mutt_default_save(char *, size_t, struct header *);
void mutt_display_address(struct envelope *);
void mutt_display_sanitize(char *);
void mutt_draw_statusline(int n, char *);
void mutt_edit_content_type(struct header *, struct body *, FILE *);
void mutt_edit_file(const char *, const char *);
void mutt_edit_headers(const char *, const char *, struct header *, char *, size_t);
int mutt_filter_unprintable(char **);
void mutt_curses_error(const char *, ...);
void mutt_curses_message(const char *, ...);
void mutt_encode_path(char *, size_t, const char *);
void mutt_enter_command(void);
void mutt_expand_aliases_env(struct envelope *);
void mutt_expand_file_fmt(char *, size_t, const char *, const char *);
void mutt_expand_fmt(char *, size_t, const char *, const char *);
void mutt_expand_link(char *, const char *, const char *);
void mutt_fix_reply_recipients(struct envelope *env);
void mutt_folder_hook(char *);
void mutt_format_string(char *, size_t, int, int, int, char, const char *, size_t, int);
void mutt_format_s(char *, size_t, const char *, const char *);
void mutt_format_s_tree(char *, size_t, const char *, const char *);
void mutt_forward_intro(FILE *fp, struct header *cur);
void mutt_forward_trailer(FILE *fp);
void mutt_free_alias(struct alias **);
void mutt_free_body(struct body **);
void mutt_free_color(int fg, int bg);
void mutt_free_enter_state(struct enter_state **);
void mutt_free_envelope(struct envelope **);
void mutt_free_header(struct header **);
void mutt_free_parameter(struct parameter **);
void mutt_free_regexp(struct regexp **);
void mutt_generate_header(char *, size_t, struct header *, int);
void mutt_help(int);
void mutt_draw_tree(CONTEXT *);
void mutt_check_lookup_list(struct body *, char *, int);
void mutt_make_attribution(CONTEXT *ctx, struct header *cur, FILE *out);
void mutt_make_forward_subject(struct envelope *env, CONTEXT *ctx, struct header *cur);
void mutt_make_help(char *, size_t, char *, int, int);
void mutt_make_misc_reply_headers(struct envelope *env, CONTEXT *ctx, struct header *cur, struct envelope *curenv);
void mutt_make_post_indent(CONTEXT *ctx, struct header *cur, FILE *out);
void mutt_merge_envelopes(struct envelope* base, struct envelope** extra);
void mutt_message_to_7bit(struct body *, FILE *);
#define mutt_mktemp(a,b) _mutt_mktemp (a, b, __FILE__, __LINE__)
void _mutt_mktemp(char *, size_t, const char *, int);
void mutt_normalize_time(struct tm *);
void mutt_paddstr(int, const char *);
void mutt_parse_mime_message(CONTEXT *ctx, struct header *);
void mutt_parse_part(FILE *, struct body *);
void mutt_perror(const char *);
void mutt_prepare_envelope(struct envelope *, int);
void mutt_unprepare_envelope(struct envelope *);
void mutt_pretty_mailbox(char *, size_t);
void mutt_pretty_size(char *, size_t, off_t);
void mutt_pipe_message(struct header *);
void mutt_print_message(struct header *);
void mutt_print_patchlist (void);
void mutt_query_exit(void);
void mutt_query_menu(char *, size_t);
void mutt_safe_path(char *s, size_t l, struct address *a);
void mutt_save_path(char *s, size_t l, struct address *a);
void mutt_score_message(CONTEXT *, struct header *, int);
void mutt_select_fcc(char *, size_t, struct header *);
#define mutt_select_file(A,B,C) _mutt_select_file(A,B,C,NULL,NULL)
void _mutt_select_file(char *, size_t, int, char ***, int *);
void mutt_message_hook(CONTEXT *, struct header *, int);
void _mutt_set_flag(CONTEXT *, struct header *, int, int, int);
#define mutt_set_flag(a,b,c,d) _mutt_set_flag(a,b,c,d,1)
void mutt_set_followup_to(struct envelope *);
void mutt_shell_escape(void);
void mutt_show_error(void);
void mutt_signal_init(void);
void mutt_stamp_attachment(struct body *a);
void mutt_tabs_to_spaces(char *);
void mutt_tag_set_flag(int, int);
short mutt_ts_capability(void);
void mutt_unblock_signals(void);
void mutt_unblock_signals_system(int);
void mutt_update_encoding(struct body *a);
void mutt_version(void);
void mutt_view_attachments(struct header *);
void mutt_write_address_list(struct address *adr, FILE *fp, int linelen, int display);
void mutt_set_virtual(CONTEXT *);

int mutt_add_to_rx_list(struct rx_list **list, const char *s, int flags, struct buffer *err);
int mutt_addr_is_user(struct address *);
int mutt_addwch(wchar_t);
int mutt_alias_complete(char *, size_t);
void mutt_alias_add_reverse(struct alias *t);
void mutt_alias_delete_reverse(struct alias *t);
int mutt_alloc_color (int fg, int bg);
int mutt_any_key_to_continue (const char *);
int mutt_buffy_check (int);
int mutt_buffy_notify (void);
int mutt_builtin_editor (const char *, struct header *, struct header *);
int mutt_can_decode (struct body *);
int mutt_change_flag (struct header *, int);
int mutt_check_alias_name (const char *, char *, size_t);
int mutt_check_encoding (const char *);
int mutt_check_key (const char *);
int mutt_check_menu (const char *);
int mutt_check_mime_type (const char *);
int mutt_check_month (const char *);
int mutt_check_overwrite (const char *, const char *, char *, size_t, int *, char **);
int mutt_check_traditional_pgp (struct header *, int *);
int mutt_command_complete (char *, size_t, int, int);
int mutt_var_value_complete (char *, size_t, int);
int mutt_complete (char *, size_t);
int mutt_compose_attachment (struct body *a);
int mutt_copy_body (FILE *, struct body **, struct body *);
int mutt_decode_save_attachment (FILE *, struct body *, char *, int, int);
int mutt_display_message (struct header *h);
int mutt_dump_variables (void);
int mutt_edit_attachment(struct body *);
int mutt_edit_message (CONTEXT *, struct header *);
int mutt_fetch_recips (struct envelope *out, struct envelope *in, int flags);
int mutt_chscmp (const char *s, const char *chs);
#define mutt_is_utf8(a) mutt_chscmp (a, "utf-8")
#define mutt_is_us_ascii(a) mutt_chscmp (a, "us-ascii")
int mutt_parent_message (CONTEXT *, struct header *);
int mutt_prepare_template(FILE*, CONTEXT *, struct header *, struct header *, short);
int mutt_resend_message (FILE *, CONTEXT *, struct header *);
#define mutt_enter_fname(A,B,C,D,E) _mutt_enter_fname(A,B,C,D,E,0,NULL,NULL,0)
#define mutt_enter_vfolder(A,B,C,D,E) _mutt_enter_fname(A,B,C,D,E,0,NULL,NULL,M_SEL_VFOLDER)

int _mutt_enter_fname (const char *, char *, size_t, int *, int, int, char ***, int *, int);
int  mutt_enter_string (char *buf, size_t buflen, int y, int x, int flags);
int _mutt_enter_string (char *, size_t, int, int, int, int, char ***, int *, struct enter_state *);
#define mutt_get_field(A,B,C,D) _mutt_get_field(A,B,C,D,0,NULL,NULL)
int _mutt_get_field (const char *, char *, size_t, int, int, char ***, int *);
int mutt_get_hook_type (const char *);
int mutt_get_field_unbuffered (char *, char *, size_t, int);
#define mutt_get_password(A,B,C) mutt_get_field_unbuffered(A,B,C,M_PASS)
int mutt_get_postponed (CONTEXT *, struct header *, struct header **, char *, size_t);
int mutt_get_tmp_attachment (struct body *);
int mutt_index_menu (void);
int mutt_invoke_sendmail (struct address *, struct address *, struct address *, struct address *, const char *, int);
int mutt_is_mail_list (struct address *);
int mutt_is_message_type(int, const char *);
int mutt_is_list_cc (int, struct address *, struct address *);
int mutt_is_list_recipient (int, struct address *, struct address *);
int mutt_is_subscribed_list (struct address *);
int mutt_is_text_part (struct body *);
int mutt_is_valid_mailbox (const char *);
int mutt_link_threads (struct header *, struct header *, CONTEXT *);
int mutt_lookup_mime_type (struct body *, const char *);
int mutt_match_rx_list (const char *, struct rx_list *);
int mutt_match_spam_list (const char *, struct spam_list *, char *, int);
int mutt_messages_in_thread (CONTEXT *, struct header *, int);
int mutt_multi_choice (char *prompt, char *letters);
int mutt_needs_mailcap (struct body *);
int mutt_num_postponed (int);
int mutt_parse_bind (struct buffer *, struct buffer *, unsigned long, struct buffer *);
int mutt_parse_exec (struct buffer *, struct buffer *, unsigned long, struct buffer *);
int mutt_parse_color (struct buffer *, struct buffer *, unsigned long, struct buffer *);
int mutt_parse_uncolor (struct buffer *, struct buffer *, unsigned long, struct buffer *);
int mutt_parse_hook (struct buffer *, struct buffer *, unsigned long, struct buffer *);
int mutt_parse_macro (struct buffer *, struct buffer *, unsigned long, struct buffer *);
int mutt_parse_mailboxes (struct buffer *, struct buffer *, unsigned long, struct buffer *);
int mutt_parse_mono (struct buffer *, struct buffer *, unsigned long, struct buffer *);
int mutt_parse_unmono (struct buffer *, struct buffer *, unsigned long, struct buffer *);
int mutt_parse_push (struct buffer *, struct buffer *, unsigned long, struct buffer *);
int mutt_parse_rc_line (/* const */ char *, struct buffer *, struct buffer *);
int mutt_parse_rfc822_line (struct envelope *e, struct header *hdr, char *line, char *p,
			    short user_hdrs, short weed, short do_2047, struct list_t **lastp);
int mutt_parse_score (struct buffer *, struct buffer *, unsigned long, struct buffer *);
int mutt_parse_unscore (struct buffer *, struct buffer *, unsigned long, struct buffer *);
int mutt_parse_unhook (struct buffer *, struct buffer *, unsigned long, struct buffer *);
int mutt_pattern_func (int, char *);
int mutt_pipe_attachment (FILE *, struct body *, const char *, char *);
int mutt_print_attachment (FILE *, struct body *);
int mutt_query_complete (char *, size_t);
int mutt_query_variables (struct list_t *queries);
int mutt_save_attachment (FILE *, struct body *, char *, int, struct header *);
int _mutt_save_message (struct header *, CONTEXT *, int, int, int);
int mutt_save_message (struct header *, int, int, int, int *);
int mutt_search_command (int, int);
#if USE_SMTP
int mutt_smtp_send (const struct address *, const struct address *, const struct address *,
                    const struct address *, const char *, int);
#endif
int mutt_wstr_trunc (const char *, size_t, size_t, size_t *);
int mutt_charlen (const char *s, int *);
int mutt_strwidth (const char *);
int mutt_compose_menu (struct header *, char *, size_t, struct header *);
int mutt_thread_set_flag (struct header *, int, int, int);
int mutt_user_is_recipient (struct header *);
void mutt_update_num_postponed (void);
int mutt_wait_filter (pid_t);
int mutt_which_case (const char *);
int mutt_write_fcc (const char *path, struct header *hdr, const char *msgid, int, char *, char **);
int mutt_write_mime_body (struct body *, FILE *);
int mutt_write_mime_header (struct body *, FILE *);
int mutt_write_one_header (FILE *fp, const char *tag, const char *value, const char *pfx, int wraplen, int flags);
int mutt_write_rfc822_header (FILE *, struct envelope *, struct body *, int, int);
void mutt_write_references (struct list_t *, FILE *, int);
int mutt_yesorno (const char *, int);
void mutt_set_header_color(CONTEXT *, struct header *);
void mutt_sleep (short);
int mutt_save_confirm (const char  *, struct stat *);

int mh_valid_message (const char *);

pid_t mutt_create_filter (const char *, FILE **, FILE **, FILE **);
pid_t mutt_create_filter_fd (const char *, FILE **, FILE **, FILE **, int, int, int);

struct address *alias_reverse_lookup (struct address *);

/* base64.c */
void mutt_to_base64 (unsigned char*, const unsigned char*, size_t, size_t);
int mutt_from_base64 (char*, const char*);

/* utf8.c */
int mutt_wctoutf8 (char *s, unsigned int c, size_t buflen);

#if LOCALES_HACK
#define IsPrint(c) (isprint((unsigned char)(c)) ||	\
		    ((unsigned char)(c) >= 0xa0))
#define IsWPrint(wc) (iswprint(wc) || wc >= 0xa0)
#else
#define IsPrint(c) (isprint((unsigned char)(c)) ||	\
		    (bit_val(options, OPTLOCALES) ? 0 :        \
		     ((unsigned char)(c) >= 0xa0)))
#define IsWPrint(wc) (iswprint(wc) ||				\
		      (bit_val(options, OPTLOCALES) ? 0 : (wc >= 0xa0)))
#endif

#define new_pattern() safe_calloc(1, sizeof (pattern_t))

int mutt_pattern_exec(struct pattern_t *pat, pattern_exec_flag flags, CONTEXT *ctx, struct header *h);
pattern_t *mutt_pattern_comp(/* const */ char *s, int flags, struct buffer *err);
void mutt_check_simple(char *s, size_t len, const char *simple);
void mutt_pattern_free(pattern_t **pat);

int getdnsdomainname(char *, size_t);
void ci_bounce_message(struct header *, int *);
int ci_send_message(int, struct header *, char *, CONTEXT *, struct header *);
int query_quadoption(int opt, const char *prompt);
