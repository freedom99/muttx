/*
 * Copyright (C) 1996-2000 Michael R. Elkins <me@mutt.org>
 * Copyright (C) 2004 g10 Code GmbH
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef _MUTT_CURSES_H_
#define _MUTT_CURSES_H_ 1

#include <ncurses.h>
#include "bitmap.h"

#define M_ENTER_C '\n'
#define M_ENTER_S "\n"

#define CLEARLINE_WIN(x) move(x,SidebarWidth), clrtoeol()
#define CLEARLINE(x) move(x,0), clrtoeol()
#define CENTERLINE(x,y) move(y, (COLS-strlen(x))/2), addstr(x)
#define BEEP() do { if (bit_val(options, OPTBEEP)) beep(); } while (0)

void mutt_curs_set (int);
#define PAGELEN (LINES-3)

#define ctrl(c) ((c)-'@')

#ifdef KEY_ENTER
#define CI_is_return(c) ((c) == '\r' || (c) == '\n' || (c) == KEY_ENTER)
#else
#define CI_is_return(c) ((c) == '\r' || (c) == '\n')
#endif

struct event mutt_getch (void);

void mutt_endwin (const char *);
void mutt_flushinp (void);
void mutt_refresh (void);
void mutt_resize_screen (void);
void mutt_ungetch (int, int);
void mutt_need_hard_redraw (void);

/* ----------------------------------------------------------------------------
 * Support for color
 */

enum
{
	MT_COLOR_HDEFAULT = 0,
	MT_COLOR_QUOTED,
	MT_COLOR_SIGNATURE,
	MT_COLOR_INDICATOR,
	MT_COLOR_STATUS,
	MT_COLOR_TREE,
	MT_COLOR_NORMAL,
	MT_COLOR_ERROR,
	MT_COLOR_TILDE,
	MT_COLOR_MARKERS,
	MT_COLOR_BODY,
	MT_COLOR_HEADER,
	MT_COLOR_MESSAGE,
	MT_COLOR_ATTACHMENT,
	MT_COLOR_SEARCH,
	MT_COLOR_BOLD,
	MT_COLOR_SIDEBAR,
	MT_COLOR_UNDERLINE,
	MT_COLOR_INDEX,
	MT_COLOR_PROGRESS,
	MT_COLOR_NEW,
	MT_COLOR_FLAGGED,
	/* please no non-MT_COLOR_INDEX objects after this point */
	MT_COLOR_INDEX_FLAGS,
	MT_COLOR_INDEX_SUBJECT,
	MT_COLOR_INDEX_AUTHOR,
	/* below here - only index coloring stuff that doesn't have a pattern */
	MT_COLOR_INDEX_COLLAPSED,
	MT_COLOR_INDEX_DATE,
	MT_COLOR_INDEX_LABEL,
	MT_COLOR_INDEX_NUMBER,
	MT_COLOR_INDEX_SIZE,
	MT_COLOR_PROMPT,
	MT_COLOR_MAX
};

typedef struct color_line
{
	regex_t rx;
	int match; /* which substringmap 0 for old behaviour */
	char *pattern;
	pattern_t *color_pattern; /* compiled pattern to speed up index color
				     calculation */
	short fg;
	short bg;
	int pair;
	struct color_line *next;
} COLOR_LINE;

#define M_PROGRESS_SIZE		(1<<0)	/* traffic-based progress */
#define M_PROGRESS_MSG		(1<<1)	/* message-based progress */

typedef struct
{
	unsigned short inc;
	unsigned short flags;
	const char* msg;
	long pos;
	long size;
	unsigned int timestamp;
	char sizestr[SHORT_STRING];
} progress_t;

void mutt_progress_init (progress_t* progress, const char *msg,
			 unsigned short flags, unsigned short inc,
			 long size);
/* If percent is positive, it is displayed as percentage, otherwise
 * percentage is calculated from progress->size and pos if progress
 * was initialized with positive size, otherwise no percentage is shown */
void mutt_progress_update (progress_t* progress, long pos, int percent);

static inline int mutt_term_width(short wrap)
{
	if (wrap < 0)
		return COLS > -wrap ? COLS + wrap : COLS;
	else if (wrap)
		return wrap < COLS ? wrap : COLS;
	else
		return COLS;
}

extern int *ColorQuote;
extern int ColorQuoteUsed;
extern int ColorDefs[];
extern COLOR_LINE *ColorHdrList;
extern COLOR_LINE *ColorBodyList;
extern COLOR_LINE *ColorStatusList;
extern COLOR_LINE *ColorIndexList;
extern COLOR_LINE *ColorIndexSubjectList;
extern COLOR_LINE *ColorIndexAuthorList;
extern COLOR_LINE *ColorIndexFlagsList;

void ci_init_color (void);
void ci_start_color (void);

#define SETCOLOR(X) bkgdset(ColorDefs[X] | ' ')
#define ATTRSET(X) bkgdset(X | ' ')

/* reset the color to the normal terminal color as defined by 'color normal ...' */
#define NORMAL_COLOR SETCOLOR(MT_COLOR_NORMAL)

#define MAYBE_REDRAW(x) if (bit_val(options, OPTNEEDREDRAW)) { unset_bit(options, OPTNEEDREDRAW); x = REDRAW_FULL; }

/* ----------------------------------------------------------------------------
 * These are here to avoid compiler warnings with -Wall under SunOS 4.1.x
 */

#endif /* _MUTT_CURSES_H_ */
