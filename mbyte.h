#ifndef _MBYTE_H
#define _MBYTE_H

#include <wchar.h>
#include <wctype.h>


void mutt_set_charset (char *charset);
extern int Charset_is_utf8;
size_t utf8rtowc (wchar_t *pwc, const char *s, size_t n, mbstate_t *_ps);
wchar_t replacement_char (void);

#endif /* _MBYTE_H */
