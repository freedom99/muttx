/*
 * Copyright(C) 1996-2007 Michael R. Elkins <me@mutt.org>
 * Copyright(C) 1999-2007 Thomas Roessler <roessler@does-not-exist.org>
 * Copyright(C) 2004 g10 Code GmbH
 * Copyright(C) 2015-2016 Alexander Kuleshov <kuleshovmail@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#define MAIN_C 1

#include "mutt.h"
#include "mutt_curses.h"
#include "keymap.h"
#include "errors.h"
#include "help.h"
#include "bitmap.h"
#if USE_SASL
#include "mutt_sasl.h"
#endif
#if USE_IMAP
#include "imap/imap.h"
#endif
#if USE_HCACHE
#include "hcache.h"
#endif

void mutt_exit(int code)
{
	mutt_endwin(NULL);	
	exit(code);
}

static void start_curses(void)
{
	km_init();
	mutt_signal_init();

	if (initscr() == NULL)
	{
		puts ("Error initializing terminal.");
		exit(RETURN_ERR_TERM);
	}
	ci_start_color();
	keypad(stdscr, TRUE);
	cbreak();
	noecho();
	typeahead(-1);       /* simulate smooth scrolling */
	meta(stdscr, TRUE);
	init_extended_keys();
}

int main(int argc, char **argv)
{
	int i = 0;
	char folder[_POSIX_PATH_MAX] = "";
	char *subject = NULL;
	char *includeFile = NULL;
	char *draftFile = NULL;
	int sendflags = 0;
	int flags = 0;
	int explicit_folder = 0;
	struct header *msg = NULL;
	struct list_t *attach = NULL;
	struct list_t *addr_to = NULL;
	struct list_t *commands = NULL;
	bool is_mutt_init = false;

	/* set default locale */
	setlocale(LC_ALL, "");
	/* initialization of main output routines with default values */
	mutt_error = mutt_message = mutt_nocurses_error;
	/* skip argv[0] */
	argc--;
	argv++;
	/* parse command line options */
	while (i < argc)
	{
		if (!strcmp(argv[i], "-v"))
		{
			show_version();
			exit(RETURN_SUCCESS);
		}
		if (!strcmp(argv[i], "-vv"))
		{
			show_version_verbose();
			exit(RETURN_SUCCESS);
		}
		if (!strcmp(argv[i], "-D")) {
			if (!is_mutt_init)
				init(commands);
			return mutt_dump_variables();
		}
		if (!strcmp(argv[i], "-A"))
		{
			i++;

			if (!is_mutt_init)
				init(commands);

			while (i < argc)
			{
				struct address *a = mutt_lookup_alias(argv[i]);

				if (a)
				{
					mutt_write_address_list(a, stdout, 0, 0);
				}
				else
					printf("%s\n", argv[i]);

				i++;
			}
			exit(RETURN_SUCCESS);
		}
		if (!strcmp(argv[i], "-Q"))
		{
			struct list_t *queries = NULL;

			if (!is_mutt_init)
				init(commands);

			while (i < argc)
			{
				if (!strcmp(argv[i], "-Q"))
					queries = mutt_add_list(queries, argv[++i]);
				i++;
			}

			if (queries)
				return mutt_query_variables(queries);
		}
		if (!strcmp(argv[i], "-s"))
		{
			i++;
			subject = argv[i];
			i++;
			continue;
		}
		if (!strcmp(argv[i], "-x"))
		{
			sendflags |= SENDMAILX;
			i++;
			continue;
		}
		if (!strcmp(argv[i], "-F"))
		{
			mutt_str_replace(&Muttrc, argv[++i]);
			i++;
			continue;
		}
		if (!strcmp(argv[i], "-f"))
		{
			if (explicit_folder)
			{
				printf("Error: '-f' can be specified only once\n");
				exit(RETURN_ERR_ARG);
			}
			strfcpy(folder, argv[++i], sizeof(folder));
			explicit_folder = 1;
			i++;
			continue;
		}
		if (!strcmp(argv[i], "-a"))
		{
			i += 1;
			attach = mutt_add_list(attach, argv[i]);
			i += 1;
			continue;
		}
		if (!strcmp(argv[i], "-e"))
		{
			int tmp = i;

			if (is_mutt_init)
				continue;

			while (tmp < argc)
			{
				/* put all `-e args` args to the `commands` list */
				if (!strcmp(argv[tmp], "-e"))
					commands = mutt_add_list(commands, argv[++tmp]);
				tmp++;
			}
			/*
			 * We already got all `-e ...` args and now
			 * we can set defaults and read init files
			 */
			init(commands);
			/* we already collect all `-e ...` args */
			is_mutt_init = true;
			/* i = -e */
			i++;
			continue;
		}
		if (!strcmp(argv[i], "-i"))
		{
			includeFile = argv[++i];
			i++;
			continue;
		}
		if (!strcmp(argv[i], "-m"))
		{
			mx_set_magic(argv[++i]);
			i++;
			continue;
		}
		if (!strcmp(argv[i], "-p"))
		{
			i++;
			sendflags |= SENDPOSTPONED;
			i++;
			continue;
		}
		if (!strcmp(argv[i], "-H"))
		{
			draftFile = argv[++i];
			i++;
			continue;
		}
		if (!strcmp(argv[i], "-b") || !strcmp(argv[i], "-c"))
		{
			int opt = i;

			if (!msg)
				msg = mutt_new_header();
			if (!msg->env)
				msg->env = mutt_new_envelope();
			if (!strcmp(argv[opt], "-b"))
				msg->env->bcc = rfc822_parse_adrlist(msg->env->bcc, argv[++i]);
			else
				msg->env->cc = rfc822_parse_adrlist(msg->env->cc, argv[++i]);
			i++;
			continue;
		}
		if (!strcmp(argv[i], "--attachlist"))
		{
			i++;
			while (i < argc && strcmp(argv[i], "--"))
			{
				attach = mutt_add_list(attach, argv[i]);
				i++;
			}
			if (!strcmp(argv[i], "--"))
			{
				mutt_error("After --attachlist option must be -- <addr>");
				mutt_usage();
				exit(RETURN_ERR_ARG);
			}
			/* skip "--" */
			i++;
			/* collect addresses after "--" */
			while (i < argc)
			{
				addr_to = mutt_add_list(addr_to, argv[i]);
				i++;
			}
			break;
		}
		/* collect 'To:' addresses */
		while (i < argc)
		{
			if (url_check_scheme(argv[i]) == U_UNKNOWN) {
				mutt_error("mutt: invalid argument %s\n", argv[i]);
				mutt_usage();
				exit(RETURN_ERR_ARG);
			}
			addr_to = mutt_add_list(addr_to, argv[i]);
			i++;
		}
		break;
	}
	/* no-curses for non-terminal session */
	if (!isatty(STDIN_FILENO))
	{
		set_bit(options, OPTNOCURSES);
		sendflags = SENDBATCH;
	}
	else
	{
		/*
		 * This must come before mutt_init() because curses needs to be started
		 * before calling the init_pair() function to set the color scheme.
		 */
		start_curses();
		/* check whether terminal status is supported */
		term_status = mutt_ts_capability();
	}

	/* Initialize crypto backends. */
	crypt_init();

	if (!is_mutt_init)
		init(commands);

	if (!bit_val(options, OPTNOCURSES))
	{
		SETCOLOR(MT_COLOR_NORMAL);
		clear();
		mutt_error = mutt_curses_error;
		mutt_message = mutt_curses_message;
	}

	/* Create the Maildir directory if it doesn't exist */
	if (!bit_val(options, OPTNOCURSES) && Maildir)
	{
		struct stat sb;
		char fpath[_POSIX_PATH_MAX];
		char msg[STRING];

		strfcpy(fpath, Maildir, sizeof(fpath));
		mutt_expand_path(fpath, sizeof(fpath));
#if USE_IMAP
		/* we're not connected yet - skip mail folder creation */
		if (!mx_is_imap(fpath))
#endif
			if (stat(fpath, &sb) == -1 && errno == ENOENT)
			{
				snprintf(msg, sizeof(msg), ("%s does not exist. Create it?"), Maildir);
				if (mutt_yesorno(msg, M_YES) == M_YES)
					if (mkdir(fpath, 0700) == -1 && errno != EEXIST)
						mutt_error( ("Can't create %s: %s."), Maildir, strerror(errno));
			}
	}

	if (sendflags & SENDPOSTPONED)
	{
		if (!bit_val(options, OPTNOCURSES))
			mutt_flushinp();
		ci_send_message(SENDPOSTPONED, NULL, NULL, NULL, NULL);
		mutt_endwin(NULL);
	}
	else if (subject || msg || sendflags || draftFile || includeFile || attach)
	{
		FILE *fin = NULL;
		char buf[LONG_STRING];
		char *tempfile = NULL, *infile = NULL;
		char *bodytext = NULL;
		int rv = 0;

		if (!bit_val(options, OPTNOCURSES))
			mutt_flushinp();

		if (!msg)
			msg = mutt_new_header();
		if (!msg->env)
			msg->env = mutt_new_envelope();

		for(; addr_to; addr_to = addr_to->next)
		{
			if (url_check_scheme(addr_to->data) == U_MAILTO)
			{
				if (url_parse_mailto(msg->env, &bodytext, addr_to->data) < 0)
				{
					if (!bit_val(options, OPTNOCURSES))
						mutt_endwin(NULL);
					fputs(("Failed to parse mailto: link\n"), stderr);
					exit(RETURN_WRONG_ADDR);
				}
			}
			else
				msg->env->to = rfc822_parse_adrlist(msg->env->to, addr_to->data);
		}

		if (!draftFile && bit_val(options, OPTAUTOEDIT) && !msg->env->to && !msg->env->cc)
		{
			if (!bit_val(options, OPTNOCURSES))
				mutt_endwin(NULL);
			fputs(("No recipients specified.\n"), stderr);
			exit(RETURN_ERR_ARG);
		}

		if (subject)
			msg->env->subject = safe_strdup(subject);

		if (draftFile)
			infile = draftFile;
		else if (includeFile)
			infile = includeFile;

		if (infile || bodytext)
		{
			if (infile)
			{
				if (mutt_strcmp("-", infile) == 0)
					fin = stdin;
				else
				{
					char path[_POSIX_PATH_MAX];

					strfcpy(path, infile, sizeof(path));
					mutt_expand_path(path, sizeof(path));
					if ((fin = fopen(path, "r")) == NULL)
					{
						if (!bit_val(options, OPTNOCURSES))
							mutt_endwin(NULL);
						perror(path);
						exit(RETURN_ERR_ARG);
					}
				}

				if (draftFile)
				{
					struct envelope *opts_env = msg->env;

					msg->env = mutt_read_rfc822_header(fin, NULL, 1, 0);
					rfc822_append(&msg->env->to, opts_env->to, 0);
					rfc822_append(&msg->env->cc, opts_env->cc, 0);
					rfc822_append(&msg->env->bcc, opts_env->bcc, 0);
					if (opts_env->subject)
						mutt_str_replace(&msg->env->subject, opts_env->subject);

					mutt_free_envelope(&opts_env);
				}
			}

			mutt_mktemp(buf, sizeof(buf));
			tempfile = safe_strdup(buf);

			/* TODO: is the following if still needed? */
			if (tempfile)
			{
				FILE *fout;

				if ((fout = safe_fopen(tempfile, "w")) == NULL)
				{
					if (!bit_val(options, OPTNOCURSES))
						mutt_endwin(NULL);
					perror(tempfile);
					safe_fclose(&fin);
					safe_free(&tempfile);
					exit(RETURN_ERR_ARG);
				}
				if (fin)
					mutt_copy_stream(fin, fout);
				else if (bodytext)
					fputs(bodytext, fout);
				safe_fclose(&fout);
			}

			if (fin && fin != stdin)
				safe_fclose(&fin);
		}

		safe_free(&bodytext);

		if (attach)
		{
			struct list_t *t = attach;
			struct body *a = NULL;

			while(t)
			{
				if (a)
				{
					a->next = mutt_make_file_attach(t->data);
					a = a->next;
				}
				else
					msg->content = a = mutt_make_file_attach(t->data);
				if (!a)
				{
					if (!bit_val(options, OPTNOCURSES))
						mutt_endwin(NULL);
					fprintf(stderr, ("%s: unable to attach file.\n"), t->data);
					mutt_free_list(&attach);
					exit(RETURN_ERR_ARG);
				}
				t = t->next;
			}
			mutt_free_list(&attach);
		}

		rv = ci_send_message(sendflags, msg, tempfile, NULL, NULL);

		if (!bit_val(options, OPTNOCURSES))
			mutt_endwin(NULL);

		if (rv)
			exit(RETURN_ERR_ARG);
	}
	else
	{

		if (!folder[0])
			strfcpy(folder, NONULL(Spoolfile), sizeof(folder));
		mutt_expand_path(folder, sizeof(folder));

		mutt_str_replace(&CurrentFolder, folder);
		mutt_str_replace(&LastFolder, folder);
		mutt_folder_hook(folder);

		if ((Context = mx_open_mailbox(folder, flags, NULL)) || !explicit_folder)
		{
			mutt_index_menu();
			if (Context)
				safe_free(&Context);
		}
#if USE_IMAP
		imap_logout_all();
#endif

#if USE_SASL
		mutt_sasl_done();
#endif
		mutt_free_opts();
		mutt_endwin(Errorbuf);
	}
	exit(RETURN_SUCCESS);
}
