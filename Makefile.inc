#
# General variables
#
MUTT_VERSION="1.6.0-rc0"
PROGNAME=mutt
UNAME = $(shell uname -s)
ifeq ($(UNAME), Linux)
	MAKE=make
else
	MAKE=gmake
endif

#
# compilation related variables
#
DEFS =
OBJECTS=
MUTT_INCLUDES=-I. -I/usr/local/include

#
# Debug mode
#
ifdef DEBUG
CFLAGS=-g
else
CFLAGS=-O2
endif

AR:=ar
ARFLAGS = cr
CC:=gcc
RANLIB:=ranlib
CFLAGS+=-Wunused -Wall -pedantic -Wno-long-long
COMPILE = @$(CC) $(DEFS) $(MUTT_INCLUDES) $(CFLAGS)
STRIPT?=strip

#
# script/ related variables
#
SCRIPTS_DIR=scripts
GETCONF=get_conf_val
GET_CONF_VAL=./$(SCRIPTS_DIR)/$(GETCONF)

#
# Directories
#
ALG_DIR=./alg
CRYPTO_DIR=./crypto
RFC_DIR=./rfc
IMAP_DIR=./imap
DATA_STRUCTS_DIR=./alg/data

LRFC  = -Lrfc -lrfc
LIMAP = -Limap -limap
LALG = -Lalg -lalg
LCRYPTO = -Lcrypto -lmuttcrypto
MUTT_LIBS=$(LCRYPTO) $(LRFC) $(LIMAP) $(LALG)
LIBS = $(SYSTEM_LIBS) -lncursesw -lssl -lcrypto -lz -lgdbm -lsasl2 $(MUTT_LIBS)
SYSTEM_LIBS=-L/usr/local/lib -L/usr/include -L/usr/lib -L/usr/lib64

#
# Libraries
#
LIBRFC=librfc.a
LIBIMAP=libimap.a
LIBCRYPTO=libmuttcrypto.a
LIBALG=libalg.a
LIB_DATA_STRUCTS=libdata_structs.a
LOCAL_LIBS=$(LIBALG) $(LIBCRYPTO) $(LIBRFC) $(LIBIMAP)
