muttx
===================

The `muttx` is the next generation of the [mutt](http://www.mutt.org/)
text-based mail client for Unix operating systems.

Note that this fork is based on the [mutt-kz](https://github.com/karelzak/mutt-kz).

Goals
===================

The original [mutt](http://www.mutt.org/) is the best text-base mail client
that I have seen. But it is not ideal. At least for me. Actually there are
main reasons why I've forked it:

1. Many thanks to [GNU](https://www.gnu.org/), but I hate your coding style
[style](http://www.gnu.org/prep/standards/standards.html).

2. I want to make it modern. This includes support for multithreading (yes,
I know, it has support of mutlithreading, but I want to delete an email and
move next, but I don't want to wait while it will delete an email). More
powerful sidebar. Built-in mail editor and in the end up make it really fast.

Some screenshots
===================

TODO

How to use
===================

TODO

License
===================

See `LICENSE` file - [GPL V2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html) 

Author
===================

All [mutt](http://www.mutt.org/) authors and some contributions by [@0xAX](https://twitter.com/0xAX).
