/*
 * Copyright (C) 2015-2016 Alexander Kuleshov <kuleshovmail@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#pragma once

#include "mutt.h"

#define BIT_NUMBER(bit) (bit & 7)
#define BYTE_IN_BITMAP(bitmap, bit) bitmap[bit >> 3]

#define set_bit(bitmap, bt)     \
        BYTE_IN_BITMAP(bitmap, bt)  |=  (1 << BIT_NUMBER(bt))

#define unset_bit(bitmap, bt)   \
        BYTE_IN_BITMAP(bitmap, bt)  &= ~(1 <<  BIT_NUMBER(bt))

#define toggle_bit(bitmap, bt)  \
        BYTE_IN_BITMAP(bitmap, bt)  ^=  (1 << BIT_NUMBER(bt))

#define bit_val(bitmap, bt)     \
        (BYTE_IN_BITMAP(bitmap, bt)   &  (1 << BIT_NUMBER(bt)))

static inline void toggle_quadoption(int opt)
{
	int b = (opt % 4) << 1;

	quad_options[opt >> 2] ^= (1 << b);
}

static inline void set_quadoption(int opt, int flag)
{
	int n = opt >> 2;
	int b = (opt % 4) << 1;

	quad_options[n] &= ~(0x3 << b);
	quad_options[n] |= (flag & 0x3) << b;
}

static inline int quadoption(int opt)
{
	int b =(opt % 4) << 1;

	return(quad_options[opt >> 2] >> b) & 0x3;
}
