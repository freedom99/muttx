/*
 * Copyright(C) 2004 g10 Code GmbH
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CRYPT_GPGME_H
#define CRYPT_GPGME_H

#include "mutt_crypt.h"

void pgp_gpgme_init(void);
void smime_gpgme_init(void);

char *pgp_gpgme_findkeys(struct address *adrlist, int oppenc_mode);
char *smime_gpgme_findkeys(struct address *adrlist, int oppenc_mode);

struct body *pgp_gpgme_encrypt_message(struct body *a, char *keylist, int sign);
struct body *smime_gpgme_build_smime_entity(struct body *a, char *keylist);

int pgp_gpgme_decrypt_mime(FILE *fpin, FILE **fpout, struct body *b, struct body **cur);
int smime_gpgme_decrypt_mime(FILE *fpin, FILE **fpout, struct body *b, struct body **cur);

int pgp_gpgme_check_traditional(FILE *fp, struct body *b, int tagged_only);
void pgp_gpgme_invoke_import(const char* fname);

int pgp_gpgme_application_handler(struct body *m, STATE *s);
int smime_gpgme_application_handler(struct body *a, STATE *s);
int pgp_gpgme_encrypted_handler(struct body *a, STATE *s);

struct body *pgp_gpgme_make_key_attachment(char *tempf);

struct body *pgp_gpgme_sign_message(struct body *a);
struct body *smime_gpgme_sign_message(struct body *a);

int pgp_gpgme_verify_one(struct body *sigbdy, STATE *s, const char *tempfile);
int smime_gpgme_verify_one(struct body *sigbdy, STATE *s, const char *tempfile);

int pgp_gpgme_send_menu(struct header *msg, int *redraw);
int smime_gpgme_send_menu(struct header *msg, int *redraw);

int smime_gpgme_verify_sender(struct header *h);

void gpgme_set_sender(const char *sender);

#endif
