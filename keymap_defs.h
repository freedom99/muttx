#ifdef HELP_C
const char *HelpStrings[] = {
	("null operation"),
	("end of conditional execution (noop)"),
	("force viewing of attachment using mailcap"),
	("view attachment as text"),
	("Toggle display of subparts"),
	("move to the bottom of the page"),
	("remail a message to another user"),
	("select a new file in this directory"),
	("view file"),
	("display the currently selected file's name"),
	("subscribe to current mailbox (IMAP only)"),
	("unsubscribe from current mailbox (IMAP only)"),
	("toggle view all/subscribed mailboxes (IMAP only)"),
	("list mailboxes with new mail"),
	("change directories"),
	("check mailboxes for new mail"),
	("attach file(s) to this message"),
	("attach message(s) to this message"),
	("edit the BCC list"),
	("edit the CC list"),
	("edit attachment description"),
	("edit attachment transfer-encoding"),
	("enter a file to save a copy of this message in"),
	("edit the file to be attached"),
	("edit the from field"),
	("edit the message with headers"),
	("edit the message"),
	("edit attachment using mailcap entry"),
	("edit the Reply-To field"),
	("edit the subject of this message"),
	("edit the TO list"),
	("create a new mailbox (IMAP only)"),
	("edit attachment content type"),
	("get a temporary copy of an attachment"),
	("run ispell on the message"),
	("compose new attachment using mailcap entry"),
	("toggle recoding of this attachment"),
	("save this message to send later"),
	("rename/move an attached file"),
	("send the message"),
	("toggle disposition between inline/attachment"),
	("toggle whether to delete file after sending it"),
	("update an attachment's encoding info"),
	("write the message to a folder"),
	("copy a message to a file/mailbox"),
	("create an alias from a message sender"),
	("move entry to bottom of screen"),
	("move entry to middle of screen"),
	("move entry to top of screen"),
	("make decoded (text/plain) copy"),
	("make decoded copy (text/plain) and delete"),
	("delete the current entry"),
	("delete the current mailbox (IMAP only)"),
	("delete all messages in subthread"),
	("delete all messages in thread"),
	("display full address of sender"),
	("display message and toggle header weeding"),
	("display a message"),
	("edit the raw message"),
	("delete the char in front of the cursor"),
	("move the cursor one character to the left"),
	("move the cursor to the beginning of the word"),
	("jump to the beginning of the line"),
	("cycle among incoming mailboxes"),
	("complete filename or alias"),
	("complete address with query"),
	("delete the char under the cursor"),
	("jump to the end of the line"),
	("move the cursor one character to the right"),
	("move the cursor to the end of the word"),
	("scroll down through the history list"),
	("scroll up through the history list"),
	("delete chars from cursor to end of line"),
	("delete chars from the cursor to the end of the word"),
	("delete all chars on the line"),
	("delete the word in front of the cursor"),
	("quote the next typed key"),
	("transpose character under cursor with previous"),
	("capitalize the word"),
	("convert the word to lower case"),
	("convert the word to upper case"),
	("enter a muttrc command"),
	("enter a file mask"),
	("exit this menu"),
	("filter attachment through a shell command"),
	("move to the first entry"),
	("toggle a message's 'important' flag"),
	("forward a message with comments"),
	("select the current entry"),
	("reply to all recipients"),
	("scroll down 1/2 page"),
	("scroll up 1/2 page"),
	("this screen"),
	("jump to an index number"),
	("move to the last entry"),
	("reply to specified mailing list"),
	("execute a macro"),
	("compose a new mail message"),
	("break the thread in two"),
	("open a different folder"),
	("open a different folder in read only mode"),
	("clear a status flag from a message"),
	("delete messages matching a pattern"),
	("force retrieval of mail from IMAP server"),
	("logout from all IMAP servers"),
	("move to the first message"),
	("move to the last message"),
	("show only messages matching a pattern"),
	("link tagged message to the current one"),
	("open next mailbox with new mail"),
	("jump to the next new message"),
	("jump to the next new or unread message"),
	("jump to the next subthread"),
	("jump to the next thread"),
	("move to the next undeleted message"),
	("jump to the next unread message"),
	("jump to parent message in thread"),
	("jump to previous thread"),
	("jump to previous subthread"),
	("move to the previous undeleted message"),
	("jump to the previous new message"),
	("jump to the previous new or unread message"),
	("jump to the previous unread message"),
	("mark the current thread as read"),
	("mark the current subthread as read"),
	("set a status flag on a message"),
	("save changes to mailbox"),
	("tag messages matching a pattern"),
	("delete from mutt, don't touch on disk"),
	("undelete messages matching a pattern"),
	("untag messages matching a pattern"),
	("move to the middle of the page"),
	("move to the next entry"),
	("scroll down one line"),
	("move to the next page"),
	("jump to the bottom of the message"),
	("toggle display of quoted text"),
	("skip beyond quoted text"),
	("jump to the top of the message"),
	("pipe message/attachment to a shell command"),
	("move to the previous entry"),
	("scroll up one line"),
	("move to the previous page"),
	("print the current entry"),
	("query external program for addresses"),
	("append new query results to current results"),
	("save changes to mailbox and quit"),
	("recall a postponed message"),
	("clear and redraw the screen"),
	("{internal}"),
	("rename the current mailbox (IMAP only)"),
	("reply to a message"),
	("use the current message as a template for a new one"),
	("save message/attachment to a mailbox/file"),
	("search for a regular expression"),
	("search backwards for a regular expression"),
	("search for next match"),
	("search for next match in opposite direction"),
	("toggle search pattern coloring"),
	("invoke a command in a subshell"),
	("sort messages"),
	("sort messages in reverse order"),
	("tag the current entry"),
	("apply next function to tagged messages"),
	("apply next function ONLY to tagged messages"),
	("tag the current subthread"),
	("tag the current thread"),
	("toggle a message's 'new' flag"),
	("toggle whether the mailbox will be rewritten"),
	("toggle whether to browse mailboxes or all files"),
	("move to the top of the page"),
	("undelete the current entry"),
	("undelete all messages in thread"),
	("undelete all messages in subthread"),
	("show the Mutt version number and date"),
	("view attachment using mailcap entry if necessary"),
	("show MIME attachments"),
	("display the keycode for a key press"),
	("show currently active limit pattern"),
	("collapse/uncollapse current thread"),
	("collapse/uncollapse all threads"),
	("scroll the mailbox pane up 1 page"),
	("scroll the mailbox pane down 1 page"),
	("go down to next mailbox"),
	("go to previous mailbox"),
	("go down to next mailbox with new mail"),
	("go to previous mailbox with new mail"),
	("open hilighted mailbox"),
	("toggle between mailboxes and virtual mailboxes"),
	("iterate though mailboxes with new mail"),
	("attach a PGP public key"),
	("show PGP options"),
	("mail a PGP public key"),
	("verify a PGP public key"),
	("view the key's user id"),
	("check for classic PGP"),
	("show S/MIME options"),
	("make decrypted copy and delete"),
	("make decrypted copy"),
	("wipe passphrase(s) from memory"),
	("extract supported public keys"),
	NULL
};
#endif /* HELP_C */

enum {
	OP_NULL,
	OP_END_COND,
	OP_ATTACH_VIEW_MAILCAP,
	OP_ATTACH_VIEW_TEXT,
	OP_ATTACH_COLLAPSE,
	OP_BOTTOM_PAGE,
	OP_BOUNCE_MESSAGE,
	OP_BROWSER_NEW_FILE,
	OP_BROWSER_VIEW_FILE,
	OP_BROWSER_TELL,
	OP_BROWSER_SUBSCRIBE,
	OP_BROWSER_UNSUBSCRIBE,
	OP_BROWSER_TOGGLE_LSUB,
	OP_BUFFY_LIST,
	OP_CHANGE_DIRECTORY,
	OP_CHECK_NEW,
	OP_COMPOSE_ATTACH_FILE,
	OP_COMPOSE_ATTACH_MESSAGE,
	OP_COMPOSE_EDIT_BCC,
	OP_COMPOSE_EDIT_CC,
	OP_COMPOSE_EDIT_DESCRIPTION,
	OP_COMPOSE_EDIT_ENCODING,
	OP_COMPOSE_EDIT_FCC,
	OP_COMPOSE_EDIT_FILE,
	OP_COMPOSE_EDIT_FROM,
	OP_COMPOSE_EDIT_HEADERS,
	OP_COMPOSE_EDIT_MESSAGE,
	OP_COMPOSE_EDIT_MIME,
	OP_COMPOSE_EDIT_REPLY_TO,
	OP_COMPOSE_EDIT_SUBJECT,
	OP_COMPOSE_EDIT_TO,
	OP_CREATE_MAILBOX,
	OP_EDIT_TYPE,
	OP_COMPOSE_GET_ATTACHMENT,
	OP_COMPOSE_ISPELL,
	OP_COMPOSE_NEW_MIME,
	OP_COMPOSE_TOGGLE_RECODE,
	OP_COMPOSE_POSTPONE_MESSAGE,
	OP_COMPOSE_RENAME_FILE,
	OP_COMPOSE_SEND_MESSAGE,
	OP_COMPOSE_TOGGLE_DISPOSITION,
	OP_COMPOSE_TOGGLE_UNLINK,
	OP_COMPOSE_UPDATE_ENCODING,
	OP_COMPOSE_WRITE_MESSAGE,
	OP_COPY_MESSAGE,
	OP_CREATE_ALIAS,
	OP_CURRENT_BOTTOM,
	OP_CURRENT_MIDDLE,
	OP_CURRENT_TOP,
	OP_DECODE_COPY,
	OP_DECODE_SAVE,
	OP_DELETE,
	OP_DELETE_MAILBOX,
	OP_DELETE_SUBTHREAD,
	OP_DELETE_THREAD,
	OP_DISPLAY_ADDRESS,
	OP_DISPLAY_HEADERS,
	OP_DISPLAY_MESSAGE,
	OP_EDIT_MESSAGE,
	OP_EDITOR_BACKSPACE,
	OP_EDITOR_BACKWARD_CHAR,
	OP_EDITOR_BACKWARD_WORD,
	OP_EDITOR_BOL,
	OP_EDITOR_BUFFY_CYCLE,
	OP_EDITOR_COMPLETE,
	OP_EDITOR_COMPLETE_QUERY,
	OP_EDITOR_DELETE_CHAR,
	OP_EDITOR_EOL,
	OP_EDITOR_FORWARD_CHAR,
	OP_EDITOR_FORWARD_WORD,
	OP_EDITOR_HISTORY_DOWN,
	OP_EDITOR_HISTORY_UP,
	OP_EDITOR_KILL_EOL,
	OP_EDITOR_KILL_EOW,
	OP_EDITOR_KILL_LINE,
	OP_EDITOR_KILL_WORD,
	OP_EDITOR_QUOTE_CHAR,
	OP_EDITOR_TRANSPOSE_CHARS,
	OP_EDITOR_CAPITALIZE_WORD,
	OP_EDITOR_DOWNCASE_WORD,
	OP_EDITOR_UPCASE_WORD,
	OP_ENTER_COMMAND,
	OP_ENTER_MASK,
	OP_EXIT,
	OP_FILTER,
	OP_FIRST_ENTRY,
	OP_FLAG_MESSAGE,
	OP_FORWARD_MESSAGE,
	OP_GENERIC_SELECT_ENTRY,
	OP_GROUP_REPLY,
	OP_HALF_DOWN,
	OP_HALF_UP,
	OP_HELP,
	OP_JUMP,
	OP_LAST_ENTRY,
	OP_LIST_REPLY,
	OP_MACRO,
	OP_MAIL,
	OP_MAIN_BREAK_THREAD,
	OP_MAIN_CHANGE_FOLDER,
	OP_MAIN_CHANGE_FOLDER_READONLY,
	OP_MAIN_CLEAR_FLAG,
	OP_MAIN_DELETE_PATTERN,
	OP_MAIN_IMAP_FETCH,
	OP_MAIN_IMAP_LOGOUT_ALL,
	OP_MAIN_FIRST_MESSAGE,
	OP_MAIN_LAST_MESSAGE,
	OP_MAIN_LIMIT,
	OP_MAIN_LINK_THREADS,
	OP_MAIN_NEXT_UNREAD_MAILBOX,
	OP_MAIN_NEXT_NEW,
	OP_MAIN_NEXT_NEW_THEN_UNREAD,
	OP_MAIN_NEXT_SUBTHREAD,
	OP_MAIN_NEXT_THREAD,
	OP_MAIN_NEXT_UNDELETED,
	OP_MAIN_NEXT_UNREAD,
	OP_MAIN_PARENT_MESSAGE,
	OP_MAIN_PREV_THREAD,
	OP_MAIN_PREV_SUBTHREAD,
	OP_MAIN_PREV_UNDELETED,
	OP_MAIN_PREV_NEW,
	OP_MAIN_PREV_NEW_THEN_UNREAD,
	OP_MAIN_PREV_UNREAD,
	OP_MAIN_READ_THREAD,
	OP_MAIN_READ_SUBTHREAD,
	OP_MAIN_SET_FLAG,
	OP_MAIN_SYNC_FOLDER,
	OP_MAIN_TAG_PATTERN,
	OP_MAIN_QUASI_DELETE,
	OP_MAIN_UNDELETE_PATTERN,
	OP_MAIN_UNTAG_PATTERN,
	OP_MIDDLE_PAGE,
	OP_NEXT_ENTRY,
	OP_NEXT_LINE,
	OP_NEXT_PAGE,
	OP_PAGER_BOTTOM,
	OP_PAGER_HIDE_QUOTED,
	OP_PAGER_SKIP_QUOTED,
	OP_PAGER_TOP,
	OP_PIPE,
	OP_PREV_ENTRY,
	OP_PREV_LINE,
	OP_PREV_PAGE,
	OP_PRINT,
	OP_QUERY,
	OP_QUERY_APPEND,
	OP_QUIT,
	OP_RECALL_MESSAGE,
	OP_REDRAW,
	OP_REFORMAT_WINCH,
	OP_RENAME_MAILBOX,
	OP_REPLY,
	OP_RESEND,
	OP_SAVE,
	OP_SEARCH,
	OP_SEARCH_REVERSE,
	OP_SEARCH_NEXT,
	OP_SEARCH_OPPOSITE,
	OP_SEARCH_TOGGLE,
	OP_SHELL_ESCAPE,
	OP_SORT,
	OP_SORT_REVERSE,
	OP_TAG,
	OP_TAG_PREFIX,
	OP_TAG_PREFIX_COND,
	OP_TAG_SUBTHREAD,
	OP_TAG_THREAD,
	OP_TOGGLE_NEW,
	OP_TOGGLE_WRITE,
	OP_TOGGLE_MAILBOXES,
	OP_TOP_PAGE,
	OP_UNDELETE,
	OP_UNDELETE_THREAD,
	OP_UNDELETE_SUBTHREAD,
	OP_VERSION,
	OP_VIEW_ATTACH,
	OP_VIEW_ATTACHMENTS,
	OP_WHAT_KEY,
	OP_MAIN_SHOW_LIMIT,
	OP_MAIN_COLLAPSE_THREAD,
	OP_MAIN_COLLAPSE_ALL,
	OP_SIDEBAR_SCROLL_UP,
	OP_SIDEBAR_SCROLL_DOWN,
	OP_SIDEBAR_NEXT,
	OP_SIDEBAR_PREV,
	OP_SIDEBAR_NEXT_NEW,
	OP_SIDEBAR_PREV_NEW,
	OP_SIDEBAR_OPEN,
	OP_SIDEBAR_TOGGLE,
	OP_SIDEBAR_NEW,
	OP_COMPOSE_ATTACH_KEY,
	OP_COMPOSE_PGP_MENU,
	OP_MAIL_KEY,
	OP_VERIFY_KEY,
	OP_VIEW_ID,
	OP_CHECK_TRADITIONAL,
	OP_COMPOSE_SMIME_MENU,
	OP_DECRYPT_SAVE,
	OP_DECRYPT_COPY,
	OP_FORGET_PASSPHRASE,
	OP_EXTRACT_KEYS,
	OP_MAX
};
