/*
 * Copyright (C) 1996-2000,2007 Michael R. Elkins <me@mutt.org>
 * Copyright (C) 1999-2005,2007 Thomas Roessler <roessler@does-not-exist.org>
 * Copyright (C) 2015-2016 Alexander Kuleshov <kuleshovmail@gmail.com>
 *
 *     This program is free software; you can redistribute it
 *     and/or modify it under the terms of the GNU General Public
 *     License as published by the Free Software Foundation; either
 *     version 2 of the License, or (at your option) any later
 *     version.
 *
 *     This program is distributed in the hope that it will be
 *     useful, but WITHOUT ANY WARRANTY; without even the implied
 *     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *     PURPOSE.  See the GNU General Public License for more
 *     details.
 *
 *     You should have received a copy of the GNU General Public
 *     License along with this program; if not, write to the Free
 *     Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *     Boston, MA  02110-1301, USA.
 */

#ifndef _LIB_H
#define _LIB_H

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <limits.h>
#include <stdarg.h>
#include <signal.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <fcntl.h>
#include <dirent.h>
#include <sysexits.h>

#define HUGE_STRING	5120
#define LONG_STRING     1024
#define STRING          256
#define SHORT_STRING    128

#define FMT_LEFT	0
#define FMT_RIGHT	1
#define FMT_CENTER	-1

#define NONULL(x) x ? x : ""
#define MAX(a,b) ((a) < (b) ? (b) : (a))
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define ISSPACE(c) isspace((unsigned char)c)
#define strfcpy(A,B,C) strncpy(A,B,C), *(A+(C)-1)=0

/* Flags for mutt_read_line() */
#define M_CONT		(1<<0)		/* \-continuation */
#define M_EOL		(1<<1)		/* don't strip \n/\r\n */

/*
 * this macro must check for *c == 0 since isspace(0) has unreliable behavior
 * on some systems
 */
#define SKIPWS(c) while (*(c) && isspace ((unsigned char) *(c))) c++;

/*
 * skip over WSP as defined by RFC5322.  This is used primarily for parsing
 * header fields.
 */
static inline char *skip_email_wsp(const char *s)
{
	if (s)
		return (char *)(s + strspn(s, " \t\r\n"));
	return (char *)s;
}

static inline int is_email_wsp(char c)
{
	return strchr(" \t\r\n", c) != NULL;
}

static void inline safe_free(void *ptr)
{
	void **p = (void **)ptr;
	if (*p)
	{
		free(*p);
		*p = 0;
	}
}

/* The actual library functions. */
void mutt_exit(int);
char *mutt_concatn_path(char *, size_t, const char *, size_t, const char *, size_t);
char *mutt_concat_path(char *, const char *, const char *, size_t);
char *mutt_read_line(char *, size_t *, FILE *, int *, int);
char *mutt_skip_whitespace(char *);
char *mutt_strlower(char *);
char *mutt_substrcpy(char *, const char *, const char *, size_t);
char *mutt_substrdup(const char *, const char *);
const char *mutt_basename(const char *f);

char *safe_strcat(char *, size_t, const char *);
char *safe_strncat(char *, size_t, const char *, size_t);
char *safe_strdup(const char *);
FILE *safe_fopen(const char *, const char *);

/*
 * strtol() wrappers with range checking; they return
 * 	 0 success
 * 	-1 format error
 * 	-2 overflow (for int and short)
 * the int pointer may be NULL to test only without conversion
 */
int mutt_atos(const char *, short *);
int mutt_atoi(const char *, int *);
int mutt_atol(const char *, long *);

const char *mutt_stristr(const char *, const char *);

int compare_stat(struct stat *, struct stat *);
int mutt_copy_stream(FILE *, FILE *);
int mutt_copy_bytes(FILE *, FILE *, size_t);
int mutt_rx_sanitize_string(char *, size_t, const char *);
int mutt_strcasecmp(const char *, const char *);
int mutt_strcmp(const char *, const char *);
int mutt_strncasecmp(const char *, const char *, size_t);
int mutt_strncmp(const char *, const char *, size_t);
int mutt_strcoll(const char *, const char *);
int safe_asprintf(char **, const char *, ...);
int mutt_rmtree(const char *);

int safe_open(const char *, int);
int safe_rename(const char *, const char *);
int safe_symlink(const char *, const char *);
int safe_fclose(FILE **);
int safe_fsync_close(FILE **);

size_t mutt_quote_filename(char *, size_t, const char *);
size_t mutt_strlen(const char *);

void *safe_calloc(size_t, size_t);
void *safe_malloc(size_t);
void mutt_nocurses_error(const char *, ...);
void mutt_remove_trailing_ws(char *);
void mutt_sanitize_filename(char *, short);
void mutt_str_replace(char **p, const char *s);
void mutt_str_adjust(char **p);
void mutt_unlink(const char *);
void safe_realloc(void *, size_t);
const char *mutt_strsysexit(int e);

#endif /* _LIB_H */
